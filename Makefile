# Inspired by this:
# https://www.cs.swarthmore.edu/~newhall/unixhelp/javamakefiles.html

dstdir:
	mkdir classes

JC = javac
.SUFFIXES: .java .class
.java.class: dstdir
	$(JC) $(JFLAGS) $*.java -sourcepath javasrc -d classes -classpath ~/.m2/repository/org/clojure/clojure/1.10.3/clojure-1.10.3.jar

JAVASRC = \
	javasrc/jobtech/WordContextProgress.java \
	javasrc/jobtech/TextFileSplitter.java \
	javasrc/jobtech/SegmentedWord.java \
	javasrc/jobtech/TokenizerConstants.java \
	javasrc/jobtech/IndexedSentenceTransducer.java \
	javasrc/jobtech/WordContextDatasetWriter.java \
	javasrc/jobtech/CloseableFactory.java \
	javasrc/jobtech/WordContextInputFile.java \
	javasrc/jobtech/WordContext.java


default: classes

classes: $(JAVASRC:.java=.class)

all: classes
