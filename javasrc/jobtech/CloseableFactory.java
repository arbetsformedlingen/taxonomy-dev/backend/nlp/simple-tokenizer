package jobtech;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;

public abstract class CloseableFactory<T extends Closeable> implements Closeable {
    private ArrayList<T> _closeables = new ArrayList<T>();
    
    protected abstract T newCloseableInstance() throws Exception;

    public T newInstance() throws Exception {
        T x = newCloseableInstance();
        _closeables.add(x);
        return x;
    }

    public void close() throws IOException {
        if (_closeables == null) {
            return;
        }
        for (T x: _closeables) {
            x.close();
        }
        _closeables = null;
    }
}
