package jobtech;

import java.util.Arrays;
import java.util.Map;
import clojure.lang.AFn;
import clojure.lang.IFn;
import java.util.HashMap;

public class IndexedSentenceTransducer extends AFn {
    private int[] _sentence = new int[TokenizerConstants.MAX_SENTENCE_LENGTH];
    private int _counter = 0;
    private long _lastSentenceClass = -1;
    private Map _wordIndex;
    private IFn _step;

    private WordContextProgress _progress = null;

    public IndexedSentenceTransducer(Map wordIndex, IFn step) {
        _wordIndex = new HashMap(wordIndex); // Java HashMap seems slightly faster than Clojure maps.
        _step = step;
    }

    public void setProgress(WordContextProgress p) {
        _progress = p;
    }

    private Object emitSentence(Object dst) {
        if (_counter < TokenizerConstants.MIN_SENTENCE_LENGTH) {
            _counter = 0;
            return dst;
        }
        int[] sentence = Arrays.copyOf(_sentence, _counter);
        if (_progress != null) {
            _progress.contextsDone(_counter, sentence);
        }
        _counter = 0;
        return _step.invoke(dst, sentence);
    }    

    public Object invoke() {
        return _step.invoke();
    }

    public Object invoke(Object dst) {
        return _step.invoke(emitSentence(dst));
    }

    public Object invoke(Object dst, Object segmentedWord0) {
        SegmentedWord segmentedWord = (SegmentedWord)segmentedWord0;
        if (_progress != null) {
            _progress.segmentedWord(segmentedWord);
        }
        Object index = _wordIndex.get(segmentedWord.wordString());
        if (index == null) {
            return dst;
        }
        long sentenceClass = segmentedWord.getClassCounter(TokenizerConstants.SENTENCE_CLASS);
        if (sentenceClass != _lastSentenceClass) {
            dst = emitSentence(dst);
            _lastSentenceClass = sentenceClass;
        }
        _sentence[_counter++] = ((Number)index).intValue();
        return dst;
    }
}
