package jobtech;

import clojure.lang.IFn;

public class SegmentedWord {
    private Object _word;
    private long _bytesRead;
    private long _totalBytes;
    private long[] _classCounters;
    
    public SegmentedWord(
        Object word, long bytesRead, long totalBytes,
        long[] classCounters) {
        _word = word;
        _bytesRead = bytesRead;
        _totalBytes = totalBytes;
        _classCounters = classCounters;
    }

    public long[] classCounters() {
        return _classCounters;
    }

    public long getClassCounter(int index) {
        return _classCounters[index];
    }

    public long bytesRead() {
        return _bytesRead;
    }

    public long totalBytes() {
        return _totalBytes;
    }

    public Object word() {
        return _word;
    }

    public String wordString() {
        return (String)_word;
    }

    public SegmentedWord copy() {
        return new SegmentedWord(_word, _bytesRead, _totalBytes, _classCounters);
    }

    public boolean isEmpty() {
        return ((String)_word).isEmpty();
    }

    public SegmentedWord updateWord(IFn f) {
        var dst = copy();
        dst._word = f.invoke(_word);
        return dst;
    }

    public String toString() {
        return _word.toString();
    }
}
