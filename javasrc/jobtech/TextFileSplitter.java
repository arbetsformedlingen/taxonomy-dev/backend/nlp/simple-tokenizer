package jobtech;

import java.lang.Iterable;
import java.util.Iterator;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import clojure.lang.IFn;
import clojure.lang.RT;
import java.io.File;
import java.util.Arrays;
import java.util.ArrayList;

public class TextFileSplitter extends CloseableFactory<BufferedReader> implements Iterable<SegmentedWord> {
    private String _filename;
    private int[] _separators;
    private int[] _separatorClasses;
    private long _filesize;
    private int _classCount;

    private static void check(boolean x, String msg) {
        if (!x) {
            throw new RuntimeException("CHECK FAILED: " + msg);
        }
    }
    
    public TextFileSplitter(String filename, int[] separators, int[] separatorClasses) throws FileNotFoundException {
        check(separators != null, "No separators");
        check(separatorClasses != null, "No separator classes");
        check(separators.length == separatorClasses.length, "Inconsistent lengths");
        
        _filename = filename;
        _separators = separators;
        _separatorClasses = separatorClasses;
        _filesize = new File(filename).length();

        {
            int maxClass = 0;
            for (int i = 0; i < _separatorClasses.length; i++) {
                maxClass = Math.max(maxClass, _separatorClasses[i]);
            }
            _classCount = 1 + maxClass;
        }
    }

    private int getSeparatorClass(int x) {
        for (int j = 0; j < _separators.length; j++) {
            if (_separators[j] == x) {
                return _separatorClasses[j];
            }
        }
        return -1;
    }

    protected BufferedReader newCloseableInstance() throws Exception {
        return new BufferedReader(new FileReader(_filename));
    }

    private static String stringFromArray(char[] temp, int n) {
        return new String(temp, 0, n);
    }

    private class WordIterator implements Iterator<SegmentedWord> {
        private BufferedReader _reader;
        private TextFileSplitter _src;
        private boolean _hasNext = true;
        private char[] _tempData = new char[16384];
        private long _bytesRead = 0;
        private long[] _classCounters;
        
        public WordIterator(TextFileSplitter src) throws Exception {
            _reader = src.newInstance();
            _src = src;
            _classCounters = new long[_src._classCount];
        }

        public boolean hasNext() {
            return _hasNext;
        }

        private SegmentedWord newWord(int n) {
            return new SegmentedWord(
                stringFromArray(_tempData, n),
                _bytesRead,
                _src._filesize,
                Arrays.copyOf(_classCounters, _classCounters.length));
        }

        public SegmentedWord next() {
            if (!_hasNext) {
                return null;
            }
            int counter = 0;
            while (true) {
                int c = 0;
                try {
                    c = _reader.read();
                } catch (IOException e) {
                    c = -1;
                }
                if (c == -1) {
                    _hasNext = false;
                    return newWord(counter);
                }
                _bytesRead++;
                int separatorClass = _src.getSeparatorClass(c);
                if (separatorClass != -1) {
                    SegmentedWord r =  newWord(counter);
                    _classCounters[separatorClass]++;
                    return r;
                }
                _tempData[counter++] = (char)c;
            }
        }
    }
    
    public Iterator<SegmentedWord> iterator() {
        try {
            return new WordIterator(this);
        } catch (Exception e) {
            return null;
        }
    }
}
