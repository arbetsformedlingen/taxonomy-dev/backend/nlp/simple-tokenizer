package jobtech;

public class TokenizerConstants {
    public static final int WORD_CLASS = 0;
    public static final int SENTENCE_CLASS = 1;
    public static final int MAX_SENTENCE_LENGTH = 16384;
    public static final int MIN_SENTENCE_LENGTH = 2;
}
