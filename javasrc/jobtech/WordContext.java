package jobtech;

import java.util.Arrays;
import clojure.lang.PersistentHashSet;
import clojure.lang.PersistentVector;
import clojure.lang.PersistentHashMap;
import clojure.lang.Keyword;


public class WordContext {
    private int[] _data;
    private int _lower;
    private int _index;
    private int _upper;
    
    public WordContext(int[] data, int lower, int index, int upper) {
        _data = data;
        _lower = lower;
        _index = index;
        _upper = upper;
    }

    private static final Keyword DATA = Keyword.intern("data");
    private static final Keyword LOWER = Keyword.intern("lower");
    private static final Keyword INDEX = Keyword.intern("index");
    private static final Keyword UPPER = Keyword.intern("upper");

    private static Object[] toObjects(int[] src) {
        int n = src.length;
        Object[] dst = new Object[n];
        for (int i = 0; i < n; i++) {
            dst[i] = src[i];
        }
        return dst;
    }
    
    public PersistentHashMap rawData() {
        return PersistentHashMap.create(new Object[]{DATA, PersistentVector.create(toObjects(_data)), LOWER, _lower, INDEX, _index, UPPER, _upper});
    }

    private int contextSize() {
        return _upper - _lower - 1;
    }

    public void getContext(int[] output) {
        int counter = 0;
        for (int i = _lower; i < _upper; i++) {
            if (i != _index) {
                output[counter++] = _data[i];
            }
        }
    }

    public int[] contextArray() {
        int[] result = new int[contextSize()];
        getContext(result);
        return result;
    }
    
    public int label() {
        return _data[_index];
    }

    public Object[] contextObjectArray() {
        int[] inds = contextArray();
        int n = inds.length;
        Object[] data = new Object[n];
        for (int i = 0; i < n; i++) {
            data[i] = inds[i];
        }
        return data;
    }

    public PersistentHashSet contextSet() {
        return PersistentHashSet.create(contextObjectArray());
    }

    public PersistentVector contextAndLabelPair() {
        return PersistentVector.create(contextSet(), label());
    }

    public String toString() {
        return "WordContext" + contextAndLabelPair().toString();
    }
}
