package jobtech;

import java.io.File;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import clojure.lang.Keyword;


public class WordContextDatasetWriter implements Closeable {
    private DataOutputStream _out;

    public WordContextDatasetWriter(File out) throws FileNotFoundException {
        _out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(out)));
    }

    
    public void writeSentence(int[] sentence) throws IOException {
        int n = sentence.length;
        for (int i = 0; i < n; i++) {
            _out.writeInt(sentence[i]);
            _out.writeShort((short)i);
        }
    }

    public void close() throws IOException {
        if (_out == null) {
            return;
        }
        _out.close();
    }
}
