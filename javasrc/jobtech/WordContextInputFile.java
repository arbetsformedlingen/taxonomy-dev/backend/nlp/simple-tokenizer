package jobtech;

import java.io.File;
import java.util.ArrayList;
import java.io.Closeable;
import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.EOFException;
import java.lang.Iterable;
import java.util.Iterator;
import java.util.Arrays;

public class WordContextInputFile extends CloseableFactory<DataInputStream> implements Iterable<WordContext> {
    private File _file = null;
    private int _halfWindowSize = 0;
    
    public WordContextInputFile(File file, int halfWindowSize) {
        _file = file;
        _halfWindowSize = halfWindowSize;
    }

    protected DataInputStream newCloseableInstance() throws Exception {
        return new DataInputStream(
            new BufferedInputStream(
                new FileInputStream(_file)));
    }

    private class ElementIterator {
        private DataInputStream _input;

        public ElementIterator(DataInputStream s) throws IOException {
            _input = s;
            step();
        }
        
        public boolean eof = false;
        public int value = 0;
        public short index = 0;

        public void step() throws IOException {
            try {
                value = _input.readInt();
                index = _input.readShort();
            } catch (EOFException e) {
                eof = true;
            }
        }
    }
    
    private class WordContextIterator implements Iterator<WordContext> {
        private ElementIterator _input;
        private int _halfWindowSize;
        private int[] _tmp = new int[TokenizerConstants.MAX_SENTENCE_LENGTH];
        private int[] _sentence = null;
        private int _counter = 0;
        private int _sentenceLength = 0;
        
        public WordContextIterator(DataInputStream input, int halfWindowSize) throws IOException {
            _input = new ElementIterator(input);
            _halfWindowSize = halfWindowSize;
            maybeReadNextSentence();
        }

        private void maybeReadNextSentence() throws IOException {
            if (_counter < _sentenceLength) {

                return;
            }            
            _counter = 0;
            _sentenceLength = 0;
            if (_input.eof) {
                _tmp = null;
                return;
            }
            while (!_input.eof && (_input.index != 0 || _sentenceLength == 0)) {
                _tmp[_sentenceLength++] = _input.value;
                _input.step();
            }
            _sentence = Arrays.copyOf(_tmp, _sentenceLength);
        }
        
        public WordContext next() {
            if (_tmp == null) {
                return null;
            }

            int lower = Math.max(_counter - _halfWindowSize, 0);
            int upper = Math.min(_counter + 1 + _halfWindowSize, _sentenceLength);
            WordContext result = new WordContext(_sentence, lower, _counter, upper);
            _counter++;
            try {
                maybeReadNextSentence();
            } catch (IOException e) {
                System.err.println("Failed to read next word context: " + e.toString());
                _tmp = null;
                return null;
            }
            return result;
        }

        public boolean hasNext() {
            return _tmp != null;
        }
    }

    public Iterator<WordContext> iterator() {
        try {
            return new WordContextIterator(newInstance(), _halfWindowSize);
        } catch (Exception e) {
            System.err.println("Cannot create WordContext iterator : " + e.toString());
            return null;
        }
    }
}
