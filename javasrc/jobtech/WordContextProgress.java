package jobtech;

import clojure.lang.IFn;
import clojure.lang.RT;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.LongUnaryOperator;
import java.util.HashMap;
import clojure.lang.Keyword;

public class WordContextProgress {
    private long _startTimeMs = 0;
    private IFn _notificationCallback = null;

    private int _notificationPeriodIters = 1000;
    private int _notificationPeriodMs = 1000;

    private long _totalBytes = 0;
    private long _bytesRead = 0;
    private long _wordCounter = 0;
    private long _sentenceCounter = 0;

    private AtomicLong _lastTimeMs = new AtomicLong();
    private AtomicLong _contextCounter  = new AtomicLong();
    
    public WordContextProgress(IFn notificationCallback) {
        _notificationCallback = notificationCallback;
        _startTimeMs = System.currentTimeMillis();
    }

    public synchronized SegmentedWord segmentedWord(SegmentedWord x) {
        _totalBytes = x.totalBytes();
        _bytesRead = x.bytesRead();
        _wordCounter = x.getClassCounter(TokenizerConstants.WORD_CLASS);
        _sentenceCounter = x.getClassCounter(TokenizerConstants.SENTENCE_CLASS);
        return x;
    }
    
    private static Keyword ELAPSED_SECONDS = Keyword.intern("elapsed-seconds");
    private static Keyword TOTAL_CONTEXT_COUNT = Keyword.intern("total-context-count");
    private static Keyword CONTEXT_COUNT = Keyword.intern("context-count");
    private static Keyword CONTEXTS_PER_SECOND = Keyword.intern("contexts-per-second");
    private static Keyword REMAINING_TIME_SECONDS = Keyword.intern("remaining-time-seconds");
    private static Keyword PROGRESS = Keyword.intern("progress");
    private static Keyword SAMPLE_DATA = Keyword.intern("sample-data");
    
    private HashMap<Object, Object> infoMap(long currentTime, Object sampleData) {
        double elapsedSeconds = (currentTime - _startTimeMs)*0.001;
        long contextCounter = _contextCounter.get();
        double contextsPerSecond = contextCounter/elapsedSeconds;
        double secondsPerContext = elapsedSeconds/contextCounter;

        double secondsPerByte = elapsedSeconds/_bytesRead;
        long remainingBytes = _totalBytes - _bytesRead;
        double remainingTimeSeconds = remainingBytes*secondsPerByte;
        
        HashMap<Object, Object> dst = new HashMap<Object, Object>();
        dst.put(ELAPSED_SECONDS, elapsedSeconds);
        dst.put(CONTEXT_COUNT, contextCounter);
        dst.put(CONTEXTS_PER_SECOND, contextsPerSecond);
        dst.put(REMAINING_TIME_SECONDS, remainingTimeSeconds);
        dst.put(PROGRESS, ((double)_bytesRead)/_totalBytes);
        dst.put(SAMPLE_DATA, sampleData);
        return dst;
    }
    
    private synchronized void notify(long currentTime, Object sampleData) {
        _notificationCallback.invoke(infoMap(currentTime, sampleData));
    }

    public void contextsDone(int n, Object sampleData) {
        long contextCount = _contextCounter.addAndGet(n);
        if (contextCount % _notificationPeriodIters == 0) {
            long currentTime = System.currentTimeMillis();
            if (currentTime == _lastTimeMs.updateAndGet(new LongUnaryOperator() {
                    public long applyAsLong(long x) {
                        return (x + _notificationPeriodMs < currentTime)? currentTime : x; 
                    }
                })) {
                notify(currentTime, sampleData);
            }
        }
    }

    // Call this for every context that gets put into the dataset
    public Object contextDone(Object x) {
        contextsDone(1, x);
        return x;
    }

    public long contextCount() {
        return _contextCounter.get();
    }
}
