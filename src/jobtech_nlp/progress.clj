(ns jobtech-nlp.progress)

(defn percent [x]
  (str (Math/round (* 100 x)) "%"))

(defn segment-time [time-value segment-def]
  (reduce
   (fn [[value result] [label factor]]
     (if (nil? factor)
       (reverse (conj result [value label]))
       (let [n (int (/ value factor)) ]
         [n (conj result [(- value (* n factor)) label])])))
   [time-value []]
   (partition-all 2 segment-def)))

(defn render-time-segmentation [segmentation]
  (let [selected (->> segmentation
                      (drop-while (comp zero? first))
                      (take 2)
                      (take-while (comp pos? first)))]
    (->> (if (empty? selected)
           [(last segmentation)]
           selected)
         (map (fn [[v lab]] (str v " " lab)))
         (clojure.string/join ", "))))

(def time-breakdown ["seconds" 60 "minutes" 60 "hours" 24 "days"])

(defn human-readable-seconds [seconds]
  (-> seconds
      (segment-time time-breakdown)
      render-time-segmentation))

(defn time-of-keys [info ks]
  (->> ks
       (map #(get info %))
       (apply +)
       Math/round
       human-readable-seconds))

(defn display [info]
  (println (str "progress=" (-> info :progress percent)
                ", contexts/s="
                (int (:contexts-per-second info))
                ", elapsed="
                (time-of-keys info [:elapsed-seconds])
                ", remaining="
                (time-of-keys info [:remaining-time-seconds])))
  (println "   sample-data: " (pr-str (:sample-data info))))
