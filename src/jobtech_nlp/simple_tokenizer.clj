(ns jobtech-nlp.simple-tokenizer
  (:gen-class)
  (:require [clojure.core.reducers :as r]
            [clojure.java.io :as jio]
            [clojure.string :as st]
            [taoensso.nippy :as nippy]
            [jobtech-nlp.progress :as progress]
            [tech.v3.dataset :as ds]
            [tech.v3.datatype :as dtype]
            [jobtech-nlp.word-context-dataset :as dataset]
            iota)
  (:import [jobtech
            WordContextProgress
            IndexedSentenceTransducer
            TextFileSplitter]))

(set! *warn-on-reflection* true)

(def test-text "Vi söker nu en skicklig clojure-utvecklare till vårt team som arbetar med klassificering och klassificeringssystem som taxonomier och ontologier över arbetsmarknaden. Rent konkret jobbar vi med de värden som används i matchningen mellan platsannonser och CV:n som till exempel yrkesbenämningar, kompetensord, förmågor och arbetsuppgifter. Klassificeringssystemen byggs upp manuellt i nära samarbete med en redaktion och maskinellt genom att bearbeta stora mängder text.")

(def test-text2
  ["Köksbiträdes sökes"
   "Kandidater söker tjänsten direkt till arbetsgivare via mail\n\n\n\nLilla Rött har som mål att alltid ha de bästa råvarorna. Vi är helt enkelt pizzerian med det lilla extra..\n\n\nDet är meriterande om du har erfarenheter sedan tidigare, men är inget krav.\n\n\nUppgifter om företaget:\n\nTulegatan 5\nSUNDBYBERG"
   "bil vårdare"
   "Natural Science teachers"
   "\nInternational company located in Stockholm is looking for a science teacher to teach around 20 hours per week.\n\nSubjects: Biology, Chemistry, Anatomy, Physics.\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
   "Entreprenadchef"
   "\nVi söker en Entreprenadchef med minst 5 års erfarenhet som kan flytande ryska och engelska i tal och skrift. Du behöver ha ekonomisk och administrativ erfarenhet.\n\nDu utgår från kontoret i norra Stockholm och har hela Sverige som arbetsområde.\n"
   "Psykolog "])

(def test-sentence ["Klassificeringssystemen"
                    "byggs"
                    "upp"
                    "manuellt"
                    "i"
                    "nära"
                    "samarbete"
                    "med"
                    "en"
                    "redaktion"
                    "och"
                    "maskinellt"
                    "genom"
                    "att"
                    "bearbeta"
                    "stora"
                    "mängder"
                    "text"])

(def test-text3 "ABC 123. HAHA!\n\n Soligt? Mystik i Lund")

(def group-separator \)
(def record-separator \)
(def unit-separator \)

(defn ^TextFileSplitter text-file-splitter [filename]
  (TextFileSplitter. filename
                     (int-array (map int [unit-separator record-separator]))
                     (int-array [0 1])))

(defn count-parts [filename separator]
  (println "Counting parts using text file splitter...")
  (-> filename
      text-file-splitter
      seq
      count
      time))

(def newline-token "<NEWLINE>")

(def sentence-delimiters
  #{"!" "?" "." \newline newline-token})

(def token-delimiters
  #{\! \?  \. \, \space \newline})

(defn replace-newline [c]
  (if (or (= c \newline) (= c \n) (= c "\n"))
    newline-token
    c))

(defn space? [s]
  (or (= " " s)
      (= "" s)))

(defn add-to-tokens [tokens token]
  (conj tokens (replace-newline token)))

(defn delimiter? [d]
  (contains? token-delimiters (first d)))

(defn process-head [tokens current-token head]
  (cond
    (and (space? current-token) (space? head))  [tokens ""]

    (and (space? current-token) (not (space? head)) (delimiter? head))
    [(add-to-tokens tokens (str head)) ""]

    (and (not (space? head)) (not (delimiter? head))
         (not (space? current-token)) (not (delimiter? current-token)))
    [tokens (str current-token head)]

    (and (not (space? head)) (not (delimiter? head))
         (not (space? current-token)) (delimiter? current-token))
    [(add-to-tokens tokens current-token) (str head)]

    (and (space? head) (delimiter? head) (not (space? current-token)))
    [(add-to-tokens tokens current-token) ""]

    (and (space? current-token) (not (space? head)) (not (delimiter? head)))
    [tokens (str head)]

    (and (delimiter? head) (not (space? head))  (not (space? current-token)))
    [(add-to-tokens tokens current-token) (str head)]))

(defn process-empty-more [tokens current-token head]
  (if (not (space? current-token))
    (add-to-tokens tokens current-token)
    tokens))

(defn tokenize-text [text]
  (let [lower-case-text (st/lower-case text)]
    (loop [tokens []
           current-token ""
           head (first lower-case-text)
           more (next lower-case-text)]
      (let [[new-tokens new-current-token] (process-head tokens current-token (str head))
            new-head (first more)
            new-more (next more)]

        (if more
          (recur new-tokens new-current-token new-head new-more)
          (process-empty-more new-tokens new-current-token new-head))))))

(defn punctuation? [c]
  (or
   (.equals \. c)
   (.equals \? c)
   (.equals \! c)))

(defn last-char [^String s]
  (if   (< 0 (.length s))
    (.charAt s (- (.length s) 1))
    nil))

(defn process-head-2! [^StringBuilder tokens head]
  (if (.equals \space head)
    (.append tokens unit-separator)
    (if  (and (.equals \newline head)
              (not (.equals ^Character record-separator (last-char tokens))))
      (.append tokens record-separator)
      (if (punctuation? head)
        (do
          (.append tokens record-separator))
        (if (.equals \, head)
          (do
            (.append tokens unit-separator))

          (if (not (.equals \newline head))
            (.append tokens head)))))))

;; tokenizing and sentence split


(defn tokenize-text-2 [text]

  (loop [tokens (new StringBuilder)
         head (first text)
         more (next text)]
    (if more
      (do
        (process-head-2! tokens head)
        (recur tokens  (first more) (next more)))
      (do
        (process-head-2! tokens head)
        (.toString tokens)))))

(def tokenized-ads "/home/jafhk/utveckling/data/ads/out/2019-sentences-record-sep.txt")


;; You need to create this file with jq, from downloaded data see instructions in README.md
(def raw-ads-as-text-filename "raw-ads.txt")


(def tokenized-text-filename "tokenized-text.txt")
(def token-index-filename "token-index.nippy")
(def center-word-and-contexts-filename "center-word-and-contexts.tsv.gz")
(def word-context-filename "word_contexts.dat")

(def all-ads-text "/home/jafhk/utveckling/data/ads/2020-2.txt")
(defn load-job-ads [filename] (iota/seq filename))

(defn process-string [string]
  (-> string
      (st/lower-case)
      (st/replace  #"\\n" "\n")
      (st/replace #"\"" "")
      (tokenize-text-2)))

(defn save-list-of-lists-to-text-file
  "Saves a list to a text file. Writes one element a row"
  [a-list filename]
  (with-open [wrtr
              (jio/writer filename :append true)]
    (binding [*out* wrtr]
      (doseq [element a-list]
        (println element)))))

(defn save-tokenized-text-record-separated-text-file
  "Saves a list to a text file. Writes one element a row"
  [a-list filename]
  (with-open [w (clojure.java.io/writer filename)]
    (doseq [element a-list]
      (.write w (str element record-separator)))))

(defn save-dataset! [data file]
  (ds/write! (ds/->dataset data) file)
  )

(defn job-ads-to-sentences [filename]
  (->> filename
       load-job-ads
       (r/map process-string)
       (r/fold conj)))

(defn parse-all-ads [workdir jobads-filename]
  (save-tokenized-text-record-separated-text-file
   (job-ads-to-sentences jobads-filename)
   (str workdir tokenized-text-filename)))

(def buffer-size-256K 262144)

;; "/home/jafhk/utveckling/data/ads/out/2019-sentences-record-sep.txt"

(defn load-tokenized-data-from-file [filename]
  (iota/seq filename buffer-size-256K (int record-separator)))

(defn load-tokenized-data
  ([workdir tokenized-text-f]
   (load-tokenized-data-from-file (str workdir tokenized-text-f)))
  ([workdir]
   (load-tokenized-data workdir tokenized-text-filename)
   ))

(defn count-words
  ([] {})
  ([freqs word]
   (assoc freqs word (inc (get freqs word 0)))))

(defn merge-counts
  ([] {})
  ([& m] (apply merge-with + m)))

(def regexp-unit-pattern (re-pattern (str unit-separator)))

(defn split-on-unit [string]
  (st/split string regexp-unit-pattern))

(defn word-frequency [tokenized-texts]
  (r/fold merge-counts count-words
          (r/mapcat split-on-unit
                    (r/filter identity tokenized-texts))))

(defn nippy-freeze-to-file!
  [data filename]
  (let [_ (dorun data)]
    (nippy/freeze-to-file (jio/file filename) data)))


(defn words-to-token-index-map [words]
  (into {} (map #(vector (words %) %) (range (count words)))))

(defn count-words-in-all-tokenized-texts [workdir]
   (word-frequency (load-tokenized-data workdir))
  )

(defn count-words-and-freeze [workdir]
  (nippy-freeze-to-file!
   (words-to-token-index-map
    (mapv first (drop 20 (take 40020
                               (reverse
                                (sort-by
                                 second
                                 (into [] (count-words-in-all-tokenized-texts workdir))))))))
   (str workdir token-index-filename)))


(defn nippy-thaw-from-file [filename]
  (nippy/thaw-from-file (jio/file filename)))

(defn sentences-to-center-word-and-context-window [in]
  (load-tokenized-data in))

(comment
  "
   parse tokenized texts into center word and context
    nippy result
    copy result to gpu instance
   train new model")

(defn sentence-to-center-word-and-contexts [sentence half-window-size]
  (let [center-fun (fn [index word]
                     (let [[left right]   (split-at index sentence)
                           window-left    (take-last half-window-size left)
                           window-right   (take half-window-size (next right))]
                       (into {:y word}
                             (map-indexed (fn [i e] [(keyword (str "x" i)) e])
                                          (into window-left window-right)))))]
    (vec (filter #(and (:x0 %) (:y %)) (map-indexed center-fun sentence)))))

(defn sentence-to-indexes [sentence token-index-map]
  (let [tokens (split-on-unit sentence)]
    (filter identity (map #(get token-index-map %) tokens))))

(defn contexts-from-sentences-baseline [segmented-words-filenames token-index-map {:keys [half-window-size]}]
  {:pre [(number? half-window-size)]}
  (let [wcp (WordContextProgress. progress/display)]
    (->> segmented-words-filenames
         (mapcat load-tokenized-data-from-file)
         (filter identity)
         (mapcat #(-> %
                      (sentence-to-indexes token-index-map)
                      (sentence-to-center-word-and-contexts half-window-size)))
         (map #(.contextDone wcp %)))))

(def default-settings {:half-window-size 10})

(defn produce-dataset-optimized [dst-file segmented-words-filename token-index-map]
  (let [wcp (WordContextProgress. progress/display)]
    (with-open [input (text-file-splitter segmented-words-filename)]
      (transduce
       #(doto (IndexedSentenceTransducer. token-index-map %)
          (.setProgress wcp))
       dataset/write-sentence!
       (dataset/writer dst-file)
       (seq input)))
    (println "Dataset of size" (.contextCount wcp) "contexts produced.")))

(defn produce-dataset-refimpl [workdir tokenized-data-filenename]
  (let [token-index-map (nippy-thaw-from-file (str workdir token-index-filename))
        filenames [(str (jio/file workdir tokenized-data-filenename))]
        result (contexts-from-sentences-baseline filenames token-index-map default-settings)]
    (save-dataset! result (str workdir center-word-and-contexts-filename))))

(defn produce-dataset-optimized-top [workdir tokenized-data-filename]
  (let [token-index-map (nippy-thaw-from-file (str workdir token-index-filename))
        in-filename (str (jio/file workdir tokenized-data-filename))
        out-file (jio/file workdir word-context-filename)]
    (produce-dataset-optimized out-file in-filename token-index-map)
    (with-open [f (dataset/word-context-input-file out-file (:half-window-size default-settings))]
      (println "Example value (the 100th): " (-> f
                                                 seq
                                                 (nth 100)
                                                 dataset/word-context-data)))))



(defmacro case-op [args-expr & cases]
  (let [case-pairs (partition 2 cases)]
    `(case (first ~args-expr)
       ~@(apply concat
                (for [[[name & vars] body] case-pairs]
                  [name `(let [~(vec vars) (rest ~args-expr)] ~body)])))))

(def main-mode :count)

(defn -main
  "Start with args, workdir must end with /"
  [& args]
  (case-op args
           
           ["tokenize" workdir jobads-filename] (parse-all-ads workdir jobads-filename)
           
           ["wordcount" workdir] (count-words-and-freeze workdir)
           
           ["context" workdir tokenized-text-f]
           (produce-dataset-refimpl workdir tokenized-text-f)

           ["context-opt" workdir tokenized-text-f]
           (produce-dataset-optimized-top workdir tokenized-text-f)))





;;;------- Experiments -------





(comment

  (def old-data (produce-contexts :sample "work/" "tokenized-text.txt" contexts-from-sentences-baseline))
  (def new-data (produce-contexts :sample "work/" "tokenized-text.txt" contexts-from-sentences-optimized))

  (= old-data new-data)
  )
