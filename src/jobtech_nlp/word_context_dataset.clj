(ns jobtech-nlp.word-context-dataset
  (:require [clojure.java.io :as io])
  (:import [jobtech
            WordContextDatasetWriter
            WordContext
            WordContextInputFile]
           [java.io File]))


;;;------- Writing a dataset -------

(defn ^WordContextDatasetWriter writer
  "Construct a new dataset writer from a file or filename"
  [file]
  (WordContextDatasetWriter. (io/file file)))

(defn write-sentence!
  "Write a sentence of indexed words to the dataset writer"
  ([^WordContextDatasetWriter writer sentence]
   (do (.writeSentence writer sentence)
       writer))
  ([^WordContextDatasetWriter writer]
   (do (.close writer)
       writer)))


;;;------- Reading a dataset -------

(defn ^WordContextInputFile word-context-input-file
  "Constructs a `WordContextInputFile`. Call `seq` on the instance to get a lazy sequence for the dataset. You may want construct with instance using `with-open` in order to close all it when it is no longer needed."
  [file-or-filename half-window-size]
  (WordContextInputFile. (io/file file-or-filename) half-window-size))

;;;------- Word context -------

(defn word-context?
  "Test if something is a WordContext."
  [x]
  (instance? WordContext x))

(defn context-set
  "Get the set of context words of a WordContext"
  [^WordContext x]
  (.contextSet x))

(defn label
  "Get the label, that is the word we want to predict."
  [^WordContext x]
  (.label x))

(defn word-context-data
  "Get the set of context words and the label as a pair."
  [^WordContext x]
  (.contextAndLabelPair x))

(defmethod print-method WordContext [object writer]
  (.append writer (str object)))

(comment

  (def file (File/createTempFile "sample" ".dat"))
  
  (with-open [w (writer file)]
    (write-sentence! w (int-array [1 2 3 4]))
    (write-sentence! w (int-array [9 14 17])))

  (with-open [f (word-context-input-file file 1)]
    (println (seq f)))




  )



