(ns jobtech-nlp.java-test
  (:require [clojure.test :refer :all]
            iota)
  (:import [jobtech TextFileSplitter IndexedSentenceTransducer SegmentedWord]))


(def filename "resources/bible_extract.txt")
(def buffer-size-256K 262144)
(def sep (int \space))
(def word-class 0)
(def splitter (TextFileSplitter. filename
                                 (int-array [sep])
                                 (int-array [word-class])))

(deftest split-test
  (let [data (seq splitter)
        parts (map #(.word %) data)
        [a b] parts
        [d c] (reverse parts)

        x (first data)
        y (last data)
        ]
    (is (= [a b c d] ["In" "the" "waters." "\n"]))
    (is (= (iota/seq filename buffer-size-256K sep) parts))
    
    (is (= "In" (.word x)))
    (is (= 3 (.bytesRead x)))
    (is (= 573 (.totalBytes x)))
    (is (= 0 (.getClassCounter x 0)))

    (is (= "\n" (.word y)))
    (is (= 573 (.bytesRead y)))
    (is (= 573 (.totalBytes y)))
    (is (= 112 (.getClassCounter y 0)))))

(defn sword [word sentence-class]
  (SegmentedWord. word 0 0 (long-array [0 sentence-class])))

(def word-index {"du" 3
                 "ska" 4
                 "vara" 5
                 "clojure" 6
                 "bra" 7})

(deftest test-indexed-sentence-transducer
  (let [result (into [] #(IndexedSentenceTransducer. word-index %) [(sword "du" 0)
                                                                    (sword "ska" 0)
                                                                    (sword "kunna" 0)
                                                                    (sword "clojure" 0)

                                                                    (sword "du" 1)
                                                                    (sword "ska" 1)
                                                                    (sword "vara" 1)
                                                                    (sword "bra" 1)])]
    (is (every? (partial instance? (class (int-array []))) result))
    (is (= [[3 4 6] [3 4 5 7]] (map vec result)))))
