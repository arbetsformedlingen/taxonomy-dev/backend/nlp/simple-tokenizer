(ns jobtech-nlp.word-context-dataset-test
  (:require [clojure.test :refer :all]
            [jobtech-nlp.word-context-dataset :refer :all])
  (:import [java.io File]))

(def bytes-per-word (+ 4 2 2))

(defn check-dataset1 [ds]
  (is (= 5 (count ds)))
  (is (= [#{3} 2] (-> ds (nth 0) word-context-data)))
  (is (= [#{3} 2] (-> ds (nth 0 :katt) word-context-data)))
  (is (= :katt (-> ds (nth 1000 :katt))))
  (is (= [#{2 4} 3] (-> ds (nth 1) word-context-data)))
  (is (= [#{5} 7] (-> ds (nth 4) word-context-data))))

(deftest writer-test
  (let [file (File/createTempFile "ds0" ".dat")]
    (with-open [w (writer file)]
      (write-sentence! w (int-array [2 3 4]))
      (write-sentence! w (int-array [5 7])))
    (with-open [f (word-context-input-file file 1)]
      (check-dataset1 (seq f)))))
